import {Item} from '../src/item';
import {GildedTros} from '../src/gilded-tros';

const qualityCannotExceed50Test = (itemName: string) => {
    const items: Item[] = [new Item(itemName, 2, 50)];
    const app: GildedTros = new GildedTros(items);
    app.updateQuality();

    expect(app.items[0].sellIn).toEqual(1);
    expect(app.items[0].quality).toEqual(50);

    app.updateQuality();

    expect(app.items[0].sellIn).toEqual(0);
    expect(app.items[0].quality).toEqual(50);
}

describe('GildedTrosTest', () => {
    it('should add a new (standard) item', () => {
        const items: Item[] = [new Item('Cold Beer', 10, 10)];
        const app: GildedTros = new GildedTros(items);
        expect(app.items[0].name).toEqual('Cold Beer');
        expect(app.items[0].sellIn).toEqual(10);
        expect(app.items[0].quality).toEqual(10);
    });
});

describe('defaultItems', () => {
    it('should decrease sellIn and quality', () => {
        const items: Item[] = [new Item('Cold Beer', 10, 10)];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(9);
        expect(app.items[0].quality).toEqual(9);
    });

    it('should decrease sellIn but quality stays at 0', () => {
        const items: Item[] = [new Item('Cold Beer', 1, 1)];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(0);
        expect(app.items[0].quality).toEqual(0);

        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(-1);
        expect(app.items[0].quality).toEqual(0);
    });

    it('should decrease quality twice as much after sell date', () => {
        const items: Item[] = [new Item('Cold Beer', 0, 10)];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(-1);
        expect(app.items[0].quality).toEqual(8);

        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(-2);
        expect(app.items[0].quality).toEqual(6);
    });
});


describe('goodWine', () => {
    it('should decrease sellIn but increase quality', () => {
        const items: Item[] = [new Item('Good Wine', 10, 10)];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(9);
        expect(app.items[0].quality).toEqual(11);
    });

    it('quality cannot exceed 50', () => {
        qualityCannotExceed50Test('Good Wine');
    });

    it('should decrease quality twice as much after sell date', () => {
        const items: Item[] = [new Item('Good Wine', 1, 10)];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(0);
        expect(app.items[0].quality).toEqual(11);

        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(-1);
        expect(app.items[0].quality).toEqual(13);
    });
});


describe('legendaryItem', () => {
    it('should not change sellIn or quality', () => {
        const items: Item[] = [ new Item('B-DAWG Keychain', -2, 80) ];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(-2);
        expect(app.items[0].quality).toEqual(80);
    });

    it('quality can exceed 50 for legendary items', () => {
        const items: Item[] = [new Item('B-DAWG Keychain', 1, 80)];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(1);
        expect(app.items[0].quality).toEqual(80);
    });
});


describe('backstagePasses', () => {
    it('should increase quality', () => {
        const items: Item[] = [ new Item('Backstage passes for Re:Factor', 15, 10) ];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(14);
        expect(app.items[0].quality).toEqual(11);
    });

    it('increase in quality by 2 for 10 days or less', () => {
        const items: Item[] = [new Item('Backstage passes for HAXX', 9, 10)];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(8);
        expect(app.items[0].quality).toEqual(12);
    });

    it('increase in quality by 2 for 5 days or less', () => {
        const items: Item[] = [new Item('Backstage passes for HAXX', 4, 10)];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(3);
        expect(app.items[0].quality).toEqual(13);
    });

    it('should set quality to 0 if sellIn expired', () => {
        const items: Item[] = [new Item('Backstage passes for HAXX', 0, 10)];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(-1);
        expect(app.items[0].quality).toEqual(0);
    });

    it('quality cannot exceed 50', () => {
        qualityCannotExceed50Test('Backstage passes for Re:Factor');
    });
});


describe('smellyItems', () => {
    it('should decrease in quality twice as much', () => {
        const items: Item[] = [ new Item('Duplicate Code', 15, 10) ];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(14);
        expect(app.items[0].quality).toEqual(8);
    });

    it('should decrease in quality twice as much after sellIn date', () => {
        const items: Item[] = [ new Item('Ugly Variable Names', -1, 10) ];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(-2);
        expect(app.items[0].quality).toEqual(6);
    });

    it('quality cannot be lower than 0', () => {
        const items: Item[] = [new Item('Long Methods', -1, 1)];
        const app: GildedTros = new GildedTros(items);
        app.updateQuality();

        expect(app.items[0].sellIn).toEqual(-2);
        expect(app.items[0].quality).toEqual(0);
    });
});
