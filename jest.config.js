module.exports = {
  preset: 'ts-jest',
  "roots": [
    "<rootDir>/test"
  ],
  transform: {
    '^.+\\.(ts|tsx)?$': 'ts-jest',
    "^.+\\.(js|jsx)$": "babel-jest",
  }
};
