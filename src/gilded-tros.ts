import { Item } from './item';

export class GildedTros {

    public items: Array<Item>;

    private MINIMUM: number = 0;
    private MAXIMUM: number = 50;
    private smellyItems: Array<string> = [ 'Duplicate Code', 'Long Methods', 'Ugly Variable Names' ];

    constructor(items: Array<Item> = []) {
        this.items = items;
    }

    public updateQuality(): void {
        this.items = this.items.map((item: Item) => {
            // Legendary items never change value
            if (item.name === 'B-DAWG Keychain') {
                return item;
            }

            // All other items denote each day
            --item.sellIn;

            // Backstage passes increases in Quality as its SellIn value approaches
            item.quality = item.name?.toLowerCase()?.includes('backstage passes')
                ? this.modifyBackstagePassQuality(item.sellIn, item.quality)
                : this.modifyQuality(item);

            return item;
        });
    }

    private modifyQuality(item: Item): number {
        let quality = item.quality;

        // Decrease by 2 if sell by date has been passed
        let decreaseBy = item.sellIn >= 0 ? 1 : 2;

        // Smelly items degrade twice as fast
        const isSmellyItem = this.smellyItems.some(smellyItem => smellyItem === item.name);
        if (isSmellyItem) {
            decreaseBy = decreaseBy * 2;
        }

        if (item.name === 'Good Wine') {
            // Good wine increases in quality the older it gets
            quality = quality + decreaseBy;
        } else {
            // Default quality decrease
            quality = quality - decreaseBy;
        }

        return this.checkQuality(quality);
    }

    private modifyBackstagePassQuality(sellIn: number, quality: number): number {
        let updatedQuality = quality;

        if (sellIn < 0) {
            updatedQuality = 0;
        } else if (sellIn <= 5) {
            updatedQuality = updatedQuality + 3;
        } else if (sellIn <= 10) {
            updatedQuality = updatedQuality + 2;
        } else {
            updatedQuality = updatedQuality + 1;
        }

        return this.checkQuality(updatedQuality);
    }

    // This function resets the quality if it would exceed any minimum or maximum value
    private checkQuality(quality: number): number {
        if (quality <= this.MINIMUM) {
            return this.MINIMUM;
        }

        if (quality >= this.MAXIMUM) {
            return this.MAXIMUM;
        }

        return quality;
    }

}

